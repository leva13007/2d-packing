const eventsBunch = [
  {
    id: 1,
    date: 1,
    duration: 2,
  },
  {
    id: 2,
    date: 3,
    duration: 1,
  },
  {
    id: 3,
    date: 2,
    duration: 4,
  },
  {
    id: 4,
    date: 7,
    duration: 1,
  },
  {
    id: 5,
    date: 6,
    duration: 1,
  },
  {
    id: 6,
    date: 1,
    duration: 4,
  },
  {
    id: 7,
    date: 6,
    duration: 2,
  },
  {
    id: 8,
    date: 1,
    duration: 1,
  },
  {
    id: 9,
    date: 3,
    duration: 4,
  },
  {
    id: 10,
    date: 6,
    duration: 2,
  },
  {
    id: 11,
    date: 2,
    duration: 2,
  },
  {
    id: 12,
    date: 3,
    duration: 1,
  }
];
const HOURS = 12;
const rowHours = new Map([...Array(HOURS)].map((_,i) => ([i,new Map()])));
eventsBunch
  .sort((a,b) => b.duration - a.duration)
  .forEach(e => {
    const rowHourItem = rowHours.get(e.date);
    rowHourItem.set(e.id, e);
  })

// console.log("rowHours",rowHours);

let i = 0;
let rowNumber = 0;
let columnNumber = 0;
const columnsEventsGroups = new Map();
columnsEventsGroups.set(columnNumber, new Map());
let emptyRowsCount = 0;
while (true) {
  // {
  //   // console.log("i", i);
  //   i++;
  //   if (i > 90) break;
  // }

  if (rowHours.size === emptyRowsCount) {
    console.log("Finish! No data here");
    columnsEventsGroups.delete(columnNumber);
    break;
  }

  if(rowNumber > rowHours.size - 1) {
    console.log("Reset loop!");
    rowNumber = 0;
    columnNumber++;
    columnsEventsGroups.set(columnNumber, new Map());
    emptyRowsCount = 0;
  }

  const rowAsHourData = rowHours.get(rowNumber);
  // console.log('Row data: ', rowAsHourData);

  if(rowAsHourData.size === 0) {
    rowNumber++;
    emptyRowsCount++;
    continue;
  }

  const iterator = rowAsHourData.keys();
  const key = iterator.next().value;
  const firstEventInRowAsHourData = rowAsHourData.get(key);
  console.log('firstEventInRowAsHourData',firstEventInRowAsHourData);

  rowAsHourData.delete(key);
  (columnsEventsGroups.get(columnNumber)).set(key, firstEventInRowAsHourData);
  rowNumber = rowNumber + firstEventInRowAsHourData.duration;
  // rowNumber++;
}

console.log("columnsEventsGroups", columnsEventsGroups);